import java.util.Comparator;

/**
 * Created by bozkurti on 04/07/15.
 */

public class Point implements Comparable<Point> {
    public final Comparator<Point> SLOPE_ORDER = new SlopeOrder();        // compare points by slope to this point
    private int x = 0;
    private int y = 0;

    private final int BEFORE = -1;
    private final int EQUAL = 0;
    private final int AFTER = 1;

    public Point(int x, int y) {                        // construct the point (x, y)
        this.x = x;
        this.y = y;
    }

    private class SlopeOrder implements Comparator<Point>{
        public int compare(Point p1, Point p2){
            if (p1 == null || p2 == null){ throw new NullPointerException();}

            double compareResult = slopeTo(p1) - slopeTo(p2);

            if (compareResult > 0.0){
                return AFTER;
            }
            else if (compareResult < 0.0){
                return BEFORE;
            }
            else{
                return EQUAL;
            }
        }

        public boolean equals(Point p){
            if (p.x == x || p.y == y){
                return true;
            }
            else{
                return false;
            }
        }
    }

    public void draw() {                               // draw this point
        StdDraw.point(x, y);
    }

    public void drawTo(Point that){                   // draw the line segment from this point to that point
        StdDraw.line(this.x, this.y, that.x, that.y);
    }

    public String toString() {                           // string representation
        return "(" + x + ", " + y + ")";
    }

    public int compareTo(Point that) {                // is this point lexicographically smaller than that point?
        if (that == null){ throw new NullPointerException();}
        if (this.y == that.y){
            if (this.x == that.x){
                return EQUAL;
            }
            else if(this.x > that.x){
                return AFTER;
            }
            else {
                return BEFORE;
            }
        }
        else if (this.y > that.y){
            return AFTER;
        }
        else{
            return BEFORE;
        }
    }

    public double slopeTo(Point that) {                  // the slope between this point and that point
        if (that == null){ throw new NullPointerException();}
        if (this.x == that.x && this.y == that.y){
            return Double.NEGATIVE_INFINITY;
        }
        else if (this.x == that.x){
            return Double.POSITIVE_INFINITY;
        }
        else{
            return (double)(this.y - that.y)/(double)(this.x - that.x);
        }
    }

    public static void main(String[] args) {
        Point bla1 = new Point(1,1);
        Point bla2 = new Point(2,2);
        Point bla3 = new Point(3,4);

        int result = bla1.SLOPE_ORDER.compare(bla2,bla3);
    }
}
