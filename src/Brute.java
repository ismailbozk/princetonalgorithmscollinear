import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by bozkurti on 04/07/15.
 */
public class Brute {
    public static void main(String[] args){
        In ix = new In(args[0]);
        int count = ix.readInt();

        Point[] a = new Point[count];
        for (int x = 0; x < count; x++){
            a[x] = new Point(ix.readInt(), ix.readInt());
            a[x].draw();
        }

//        Point a1 = new Point(-1, -1);
//        Point a2 = new Point(0, 0);
//        Point a3 = new Point(1, 1);
//        Point a4 = new Point(2, 2);
//        Point a5 = new Point(2, 0);
//        Point a6 = new Point(0, 2);
//        Point a7 = new Point(-1, 3);
//        Point a8 = new Point(0, -6);
//        Point a9 = new Point(0, 6);
//        Point[] a = new Point[]{a1, a9, a8, a2, a3, a7, a6, a4, a5};


        for (int i = 0; i < a.length; i++){
            Point pi = a[i];
            for (int j = 0; j < a.length; j++){
                Point pj = a[j];
                for (int k = 0; k < a.length; k++){
                    Point pk = a[k];
                    if (pk.compareTo(pj) == 0 || pj.compareTo(pi) == 0 || pi.compareTo(pk) == 0){
                        continue;
                    }
                    int isKEqual = pi.SLOPE_ORDER.compare(pj, pk);//0 means equal
                    if (isKEqual != 0){
                        break;
                    }
                    for (int l = 0; l < a.length; l++){
                        Point pl = a[l];
                        if (pl.compareTo(pi) == 0 || pl.compareTo(pk) == 0 || pl.compareTo(pj) == 0){
                            continue;
                        }
                        int isLEqual = pi.SLOPE_ORDER.compare(pj, pl);
                        if (isLEqual == 0){
                            pi.drawTo(pj);
                            pj.drawTo(pk);
                            pk.drawTo(pl);

                            System.out.print(pi.toString());
                            System.out.print(" -> ");
                            System.out.print(pj.toString());
                            System.out.print(" -> ");
                            System.out.print(pk.toString());
                            System.out.print(" -> ");
                            System.out.print(pl.toString());
                            System.out.println("");
                        }
                    }
                }
            }
        }
    }
}

