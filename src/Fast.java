import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;


/**
 * Created by bozkurti on 04/07/15.
 */
public class Fast {
    private static int minCollinearItemCount = 3;//excludes the root item,in othe words total 4

    private static HashSet<List<Point>> collinearSet = new HashSet<List<Point>>();
    private static Point rootPointOfCollinear;

    private static void addCollinear(Point[] collinears){
        Quick3way.sort(collinears);
        collinearSet.add(Arrays.asList(collinears));
    }

    /**
     * Rearranges the array in ascending order, using the natural order.
     * @param a the array of Point s to be sorted
     */
    private static void quickGroupCollinears(Point[] a, Point rootPoint) {
        StdRandom.shuffle(a);
        rootPointOfCollinear = rootPoint;
        sort(a, 0, a.length - 1, rootPointOfCollinear.SLOPE_ORDER);
    }

    // quicksort the subarray a[lo .. hi] using 3-way partitioning
    private static void sort(Point[] a, int lo, int hi, Comparator compo) {
        if (hi <= lo) return;
        int lt = lo, gt = hi;
        Point v = a[lo];
        int i = lo;
        while (i <= gt) {
            int cmp = compo.compare(a[i], v);
            if      (cmp < 0) exch(a, lt++, i++);
            else if (cmp > 0) exch(a, i, gt--);
            else              i++;
        }

        int collinearsLength = (gt+1) - lt;
        if (collinearsLength >= minCollinearItemCount){
            Point[] temp = new Point[gt+2-lt];
            temp[0] = rootPointOfCollinear;
            int counter = 1;
            for (int index = lt; index <= gt; index++){
                temp[counter++] = a[index];
            }
            addCollinear(temp);
        }

        // a[lo..lt-1] < v = a[lt..gt] < a[gt+1..hi].
        sort(a, lo, lt-1, compo);
        sort(a, gt+1, hi, compo);
    }



    /***********************************************************************
     *  Helper sorting functions
     ***********************************************************************/

    // exchange a[i] and a[j]
    private static void exch(Point[] a, int i, int j) {
        Point swap = a[i];
        a[i] = a[j];
        a[j] = swap;
    }

    /**
     * Reads in a sequence of strings from standard input; 3-way
     * quicksorts them; and prints them to standard output in ascending order.
     */
    public static void main(String[] args) {
        In ix = new In(args[0]);
        int count = ix.readInt();

        Point[] a = new Point[count];
        for (int x = 0; x < count; x++){
            a[x] = new Point(ix.readInt(), ix.readInt());
            a[x].draw();
        }


//        Point a1 = new Point(-1, -1);
//        Point a2 = new Point(0, 0);
//        Point a3 = new Point(1, 1);
//        Point a4 = new Point(2, 2);
//        Point a5 = new Point(2, 0);
//        Point a6 = new Point(0, 2);
//        Point a7 = new Point(-1, 3);
//        Point a8 = new Point(0, -6);
//        Point a9 = new Point(0, 6);
//        Point[] a = new Point[]{a1, a9, a8, a2, a3, a7, a6, a4, a5};

        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);

        for (int i = 0; i < a.length; i++){
            Point rootPointOfCollinear = a[i];
            quickGroupCollinears(a, rootPointOfCollinear);
        }


        for (List<Point> collinearList : collinearSet){
            int counter = 0;
            Point prevPt = new Point(0,0);
            for (Point pt : collinearList){
                System.out.print(pt.toString());
                counter++;
                if (counter - 1 != collinearSet.size()){
                    System.out.print(" -> ");
                }
                if (counter != 0){
                    prevPt.drawTo(pt);
                }
                prevPt = pt;
            }
            System.out.println("");
        }
    }


}


